
var person = (function (){
     var details = {
        firstName:"Goska",
        lastName:"bajger"
    };

    // accountsList = [
    //     account1 = {
    //         balance : 13,
    //         currency : "PLN"
    //     },
    //     account3 = {
    //         balance : 16,
    //         currency : "PLN"
    //     }
       
    // ];

    function calculateBalance(){
        let totalMoney = 0;
        for(let i = 0; i < person.accountsList.length; i++)
        {
            totalMoney += person.accountsList[i].balance;
        }
        return totalMoney;
    };


    return {
        firstName: details.firstName,
        lastName: details.lastName,
        accountsList : [
            account1 = {
                balance : 13,
                currency : "PLN"
            },
            account3 = {
                balance : 16,
                currency : "PLN"
            }
           
        ],
        sayHello: () => {
            return 'First name: ' + person.firstName + ', ' + 'Last name: ' + person.lastName + ', number of accounts: ' + person.accountsList.length + ', total amount: ' + calculateBalance();
        },
        addAccount: (addBalance, addCurrency) => {
            return person.accountsList.push({"balance":addBalance, "currency":addCurrency})
    }
};
   
    
    //console.log(details);
   })();


  // personFactory();
   console.log(person.sayHello());
   person.addAccount(21,'PLN');
   console.log(person.sayHello());
