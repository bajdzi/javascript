var Account = function(balance, currency){
    this.balance = balance;
    this.currency = currency;
};


var person = (function (){
    var details = {
       firstName:"Goska",
       lastName:"bajger"
   };

//    accountsList = [
//        account1 = {
//            balance : 13,
//            currency : "PLN"
//        },
//        account3 = {
//            balance : 16,
//            currency : "PLN"
//        }
      
//    ];

   function calculateBalance(){
       let totalMoney = 0;
       for(let i = 0; i < person.accountsList.length; i++)
       {
           totalMoney += person.accountsList[i].balance;
       }
       return totalMoney;
   };


   return {
       firstName: details.firstName,
       lastName: details.lastName,
       accountsList: [new Account(37,"PLN"), new Account(14.8, "PLN")],
       sayHello: function () {
           return 'First name: ' + this.firstName + ', ' + 'Last name: ' + this.lastName + ', number of accounts: ' + this.accountsList.length + ', total amount: ' + calculateBalance();
       },
       addAccount: function(account){
        this.accountsList.push(account);
   }
};
  
  })();


 // personFactory();
  console.log(person.sayHello());
  person.addAccount(new Account(1000, "PLN"));
  console.log(person.sayHello());
