class Account {
    constructor(number, balance, currency) {
        this.number = number;
        this.balance = balance;
        this.currency = currency;
    }
}

class Person {
    constructor(firstName, lastName, accountsList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = accountsList;
    }
    calculateBalance() {
        let totalMoney = 0;
        for(let account of this.accountsList)
        {
            totalMoney += account.balance;
        }
        return totalMoney;
    }

    sayHello() {
        return 'First Name:' + this.firstName + ' ' + 'Last Name:' + this.lastName + ' ' + 'Number of accounts:' + this.accountsList.length + ' ' +
        'Total balance: ' + this.calculateBalance();
    }

    addAccount (account) {
        this.accountsList.push(account);
    }  
    
    findAccount(accountNumber){
        return this.accountsList.find((object) => object.number === accountNumber );
      }


    withdraw(accountNumber, amount){
        return new Promise((resolve, reject) =>{
            const foundAcc = this.findAccount(accountNumber);
            if (foundAcc && foundAcc.balance >= amount && amount>0){
                const currentMoney = foundAcc.balance - amount;
                setTimeout( () => resolve(`Account number: ${foundAcc.number}, Withdrawn: ${amount}, New balance: ${foundAcc.balance = currentMoney} `) , 3000);
            } else if(!foundAcc){
                reject('Such an account does not exist.');
            } else if(amount<0){
                reject('Invalid number.');
            } else{ 
                reject('You do not have enought money.');
            }
        });
        
            }
}


const init = ( () => {
    document.addEventListener('DOMContentLoaded', function () { 
        var person = new Person('Malgorzata', 'Bajger', []);
        person.addAccount(new Account(1, 1994, 'PLN'));
        person.addAccount(new Account(2, 58, 'PLN'));
        person.addAccount(new Account(3, 199999, 'PLN'));

        let numberInput = document.querySelector('#number');
        let amountInput = document.querySelector('#amount');
        let withdrawButton = document.querySelector('.btn.btn-primary');

        function onClicked() {
            let personNameBox = document.querySelector('.card-title');
            personNameBox.innerHTML = `${person.firstName} ${person.lastName}`;
            
            let personAccountBox = document.querySelector('.card-text');    
            for(var i = 0; i < person.accountsList.length; i++) {
                    let number = person.accountsList[i].number;
                    let balance = person.accountsList[i].balance;
                    let currency = person.accountsList[i].currency;
                    
                    let newP = document.createElement('p');
                    newP.id="account_" + number;
                    let newPValue = document.createTextNode(`Account nr: ${number} Balance: ${balance} Currency: ${currency}`);
                    
                    newP.appendChild(newPValue);
                    personAccountBox.appendChild(newP);
            };

            function withdrawal () {
                let accountNumber = parseFloat(numberInput.value);
                let amount = parseFloat(amountInput.value);

                person.withdraw(accountNumber, amount)
                    .then((success) => {
                        console.log(success);
                        let accountSelector = document.querySelector("#account_" + accountNumber);
                        let foundAcc = person.findAccount(accountNumber);
                        accountSelector.innerText = `account nr: ${foundAcc.number} amount: ${foundAcc.balance}
                        currency: ${foundAcc.currency}`;
                    })
                    .catch((failure) => {
                        console.log(failure);
                        document.getElementById("com").innerHTML=failure;
                    });
            };
            withdrawButton.addEventListener('click', withdrawal);
        }
            return {
            onClicked : onClicked()
        }
    }); 
}) (); 