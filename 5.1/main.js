class Account{
    constructor(balance, currency, number){
        this.balance = balance;
        this.currency = currency;
        this.number = number;
    }
}


class Person {
    constructor(firstName, lastName, accountsList){
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountsList = accountsList;
   };

    addAccount(addBalance, addCurrency, addNumber){
        return this.accountsList.push(new Account(addBalance, addCurrency, addNumber))
    }

    sayHello(){
        return `First name: ${this.firstName}, Last name: ${this.lastName}, Number of accounts: ${this.accountsList.length}, Total balance: ${this._calculateBalance()}`
    }

   _calculateBalance() {
    let totalMoney = 0;
    for(let account of this.accountsList)
    {
        totalMoney += account.balance;
    }
    return totalMoney;
}



   findAccount(accountNumber){
    return this.accountsList.find((object) => object.number === accountNumber );
  }

    withdraw(accountNumber, amount){
    return new Promise((resolve, reject) =>{
    const foundAcc = this.findAccount(accountNumber);
    if (foundAcc && foundAcc.balance >= amount){
        const currentMoney = foundAcc.balance - amount;
        setTimeout( () => resolve(`Account number: ${foundAcc.number}, Withdrawn: ${amount}, New balance: ${foundAcc.balance = currentMoney} `) , 3000);
    } else if(!foundAcc){
        reject('Such an account does not exist.');
    } else {
        reject('You do not have enought money.');
    }
});

    }

//    return {
//        firstName: details.firstName,
//        lastName: details.lastName,
//        accounts: [new Account(37,"PLN"), new Account(14.8, "PLN")],
//        sayHello: function () {
//            return 'First name: ' + details.firstName + ', ' + 'Last name: ' + details.lastName + ', number of accounts: ' + this.accounts.length + ', total amount: ' + calculateBalance();
//        },
//        addAccount: function(account){
//         this.accounts.push(account);
//    }
// };
};


 // personFactory();
  const marcin = new Person('Marcin', 'Gorka', [new Account(10000, 'PLN', 1), new Account(250, 'PLN', 2)]);
  console.log(marcin.sayHello());
  console.log(marcin.addAccount(5000,'PLN',3));
  console.log(marcin.sayHello());
  console.log(marcin.accountsList);
  console.log(marcin.sayHello());
  marcin.withdraw(2, 250)
    .then((success)=> console.log(success))
    .catch((err)=>console.log(err));
  //console.log(person.sayHello());
